package com.hermawan.demo.model;

import com.hermawan.demo.base.BaseEntity;
import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Entity;

@Data
@Entity
public class User extends BaseEntity {
    @Column(unique = true)
    private String userName;
    private String password;
    private String address;

    public User() {
    }

    public User(String userName, String password, String address) {
        this.userName = userName;
        this.password = password;
        this.address = address;
    }
}
